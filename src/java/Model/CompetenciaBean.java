package Model;
public class CompetenciaBean {
   private int id_competencias; 
   private String nombre;
   private String descripcion; 

    public CompetenciaBean() {
    }

    public CompetenciaBean(int id_competencias) {
        this.id_competencias = id_competencias;
    }

    public int getId_competencias() {
        return id_competencias;
    }

    public void setId_competencias(int id_competencias) {
        this.id_competencias = id_competencias;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
   
   
}