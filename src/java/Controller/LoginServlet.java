package Controller;

import DAO.Conexion;
import DAO.UsuariosDAO;
import Model.CompetenciaBean;
import Model.UsuariosBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LoginServlet extends HttpServlet {

    Conexion con;
    UsuariosBean user;
    UsuariosDAO userDAO = new UsuariosDAO(con);
    RequestDispatcher rd;
    String msg;
    boolean respuesta;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        String registro = request.getParameter("registro");
        switch (action) {
            case "login":
                login(request, response);
                break;
            case "cerrar":
                cerrar(request, response);
                break;
            case "insertar":
                insertar(request, response);
                break;
            case "leer":
                consultar(request, response);
                break;
            default:
                throw new AssertionError();
        }
    }
    HttpSession session;

    protected void login(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String usuario = request.getParameter("usuario");
        String clave = request.getParameter("clave");
        UsuariosDAO log= new UsuariosDAO(con);
        UsuariosBean user = new UsuariosBean();
        user.setUsuario(usuario);
        user.setPass(clave);
        if(log.consultarUsuario(user)){
            user.setUsuario(usuario);
            user.setPass(clave);
            session = request.getSession();
            session.setAttribute("user", usuario);
            response.sendRedirect("usuario.jsp");
        }
    }
    
    protected void cerrar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        session = request.getSession();
        session.setAttribute("usuario", null);
        RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
        rd.forward(request, response);
    }

    protected void consultar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        con = new Conexion();
        user = new UsuariosBean(0);
        userDAO = new UsuariosDAO(con);
        List<UsuariosBean> list = userDAO.consultar();
        request.setAttribute("list", list);
        request.setAttribute("msg", msg);
        rd = request.getRequestDispatcher("/view/usuario.jsp");
        rd.forward(request, response);
    }
    
    protected void insertar(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String nombre = request.getParameter("nombre");
        String apellido = request.getParameter("apellido");
        String correo = request.getParameter("correo");
        String direccion = request.getParameter("direccion");
        int edad = Integer.parseInt(request.getParameter("edad"));
        String usuario = request.getParameter("user");
        String pass = request.getParameter("pass");
        int id_competencia = Integer.parseInt(request.getParameter("id_competencia"));
        
        CompetenciaBean comp = new CompetenciaBean();
        con = new Conexion();
        user.setNombre(nombre);
        user.setApellido(apellido);
        user.setCorreo(correo);
        user.setDireccion(direccion);
        user.setEdad(edad);
        user.setUsuario(usuario);
        user.setPass(pass);
        comp.setId_competencias(id_competencia);
        user.setCompetencia(comp);
        
        respuesta = userDAO.insertar(user);
        if(respuesta){
            msg = "Datos insertados";
        }else{
            msg = "Error al insertar";
        }
        List<UsuariosBean> list = userDAO.consultar();
        request.setAttribute("msg", msg);
        request.setAttribute("list", list);
        rd = request.getRequestDispatcher("/view/usuario.jsp");
        rd.forward(request, response);
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
}
