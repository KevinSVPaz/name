<%-- 
    Document   : registroUser
    Created on : 01-24-2020, 08:38:30 AM
    Author     : jocelyn.pazusam
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <!-- Compiled and minified CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

        <!-- Compiled and minified JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    </head>
    <body>
        <div class="container">
            <form method="post" action="login?action=insertar">
                <div class="row">
                    <div class="col m3"></div>
                    <div class="col m3">Insertar nombre</div>
                    <div class="col m3"><input type="text" value="nombre"/></div>
                    <div class="col m3"></div>
                </div>
                <div class="row">
                    <div class="col m3"></div>
                    <div class="col m3">Insertar apellido</div>
                    <div class="col m3"><input type="text" value="apellido"/></div>
                    <div class="col m3"></div>
                </div>
                <div class="row">
                    <div class="col m3"></div>
                    <div class="col m3">Insertar correo</div>
                    <div class="col m3"><input type="text" value="correo"/></div>
                    <div class="col m3"></div>
                </div>
                <div class="row">
                    <div class="col m3"></div>
                    <div class="col m3">Insertar direccion</div>
                    <div class="col m3"><input type="text" value="direccion"/></div>
                    <div class="col m3"></div>
                </div>
                <div class="row">
                    <div class="col m3"></div>
                    <div class="col m3">Insertar edad</div>
                    <div class="col m3"><input type="text" value="edad"/></div>
                    <div class="col m3"></div>
                </div>
                <div class="row">
                    <div class="col m3"></div>
                    <div class="col m3">Insertar usuario</div>
                    <div class="col m3"><input type="text" value="user"/></div>
                    <div class="col m3"></div>
                </div>
                <div class="row">
                    <div class="col m3"></div>
                    <div class="col m3">Insertar clave</div>
                    <div class="col m3"><input type="text" value="pass"/></div>
                    <div class="col m3"></div>
                </div>
                <div class="row">
                    <div class="col m3"></div>
                    <div class="col m3">Insertar competencia</div>
                    <div class="col m3"><input type="text" value="id_competencia"/></div>
                    <div class="col m3"></div>
                </div>
                <div class="col m3"><input type="submit" value="Insertar"/></div>
            </form>
        </div>
    </body>
</html>
