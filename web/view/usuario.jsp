<%-- 
    Document   : usuario
    Created on : 01-24-2020, 08:38:13 AM
    Author     : jocelyn.pazusam
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <!-- Compiled and minified CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

        <!-- Compiled and minified JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="card blue "><h3>Listado de usuarios</h3></div>
            </div>
            <div class="row">
                <table>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombre del usuario</th>
                            <th>Correo</th>
                            <th>Dirección</th>
                            <th>Edad</th>
                            <th>User</th>
                            <th>Competencia</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${list}" var="us">
                            <tr>
                                <th>${us.id_usuarios}</th>
                                <th>${us.nombre} ${us.apellido}</th>
                                <th>${us.correo}</th>
                                <th>${us.direccion}</th>
                                <th>${us.edad}</th>
                                <th>${us.usuario}</th>
                                <th>${us.competencia.nombre}</th>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
            <a class="btn btn-dark btn-lg btn-block" href="view/registroUser.jsp">Nuevo</a>
        </div>
    </body>
</html>
